output "s3" {
  value = "${map(
    "bucket", aws_s3_bucket.s3_hosting.bucket,
    "arn",    aws_s3_bucket.s3_hosting.arn,
    "www_endpoint", aws_s3_bucket.s3_hosting.website_endpoint 
  )}"
}
