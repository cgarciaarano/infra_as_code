FROM python:slim

ENV TF_VERSION 0.11.10

RUN apt-get update &&\
    apt-get install -y wget unzip

WORKDIR /opt

RUN wget https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip &&\
    unzip terraform_${TF_VERSION}_linux_amd64.zip &&\
    mv terraform /usr/local/bin

RUN pip install awscli

ENTRYPOINT ["/bin/bash"]