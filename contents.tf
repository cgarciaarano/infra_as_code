resource "aws_s3_bucket_object" "index" {
  bucket = "${aws_s3_bucket.s3_hosting.bucket}"
  key    = "index.html"
  source = "html/index.html"
  content_type = "text/html"
  # etag   = "${md5(file("html/index.html"))}"
}

resource "aws_s3_bucket_object" "error" {
  bucket = "${aws_s3_bucket.s3_hosting.bucket}"
  key    = "error.html"
  source = "html/error.html"
  content_type = "text/html"
  # etag   = "${md5(file("html/error.html"))}"
}
