# Infrastructure as Code - Cloud

This repository provides an example of using Terraform to provision resources in AWS Cloud.

## Setup

This steps are needed to perform manual deployments from your laptop.

### Local install
- Install Terraform
  - Binary install from https://www.terraform.io/downloads.html
  - Managed install using [tfenv|https://github.com/tfutils/tfenv] **Recommended**
- Install awscli
  - `pip install awscli`
- Setup AWS credentials
  - Will be provided, shouldn't be present in the repository!
  - `aws configure`

### Docker
- Build image
  - `docker build -t example .`
- Run container  
  - `docker run -ti -v $(pwd):/opt example`
- Setup AWS credentials
  - Will be provided, shouldn't be present in the repository!
  - `aws configure`

## Terraform usage

Now we can start playing around with Terraform:

- Initialize Terraform
  - `terraform init`
- Plan deployment
  - `terraform plan -out=plan.out`
- Plan deployment
  - `terraform apply plan.out`
- Clean up
  - `terraform destroy`  