resource "aws_s3_bucket" "s3_hosting" {
  bucket = "my.hosting.bucket"
  acl    = "private"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}

data "aws_caller_identity" "current" {}

# Policy to allow public RO access, RW access to account members 
data "aws_iam_policy_document" "default_access" {
  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
    ]

    resources = [
      "${aws_s3_bucket.s3_hosting.arn}",
      "${aws_s3_bucket.s3_hosting.arn}/*",
    ]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:*",
    ]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }

    resources = [
      "${aws_s3_bucket.s3_hosting.arn}",
      "${aws_s3_bucket.s3_hosting.arn}/*",
    ]
  }
}

resource "aws_s3_bucket_policy" "default_access_policy" {
  bucket = "${aws_s3_bucket.s3_hosting.bucket}"
  policy = "${data.aws_iam_policy_document.default_access.json}"
}
